# Changelog

## 2 de juny de 2014

Segona edició

* S’hi han afegit una mitjana de tres activitats per poema.
* S’hi ha afegit un apèndix amb textos complementaris.
* S’hi han corregit errors de l’edició anterior.

## 30 d’agost de 2013

Primera edició
