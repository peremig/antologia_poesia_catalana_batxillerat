## Sobre la llicència d’aquesta *Antologia de la poesia catalana*

Sens prejudici dels drets que corresponen als autors dels poemes d’aquesta antologia que encara en conserven el copyright i en la mesura que ho permeti la llei, Pere MiG ha renunciat a tots els drets d’autor i els drets connexos o afins a *Antologia de la poesia catalana*, tot oferint-la sota domini públic.

