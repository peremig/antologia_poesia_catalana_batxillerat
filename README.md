# README
Edició dels 35 poemes que componen aquesta lectura obligatòria per als alumnes de batxillerat en format **org-mode** (*Emacs*) amb fragments en LaTeX.

## Sobre aquesta edició de l’«Antologia de la poesia catalana»
Aquesta edició inclou els poemes de lectura obligatòria per a l’alumnat de Batxillerat de les promocions a partir de 2013-2015.

Els poemes d’autors anteriors al segle XIX van acompanyats d’una versió en prosa actual en nota a peu de pàgina. Aquesta versió pretèn només ajudar a la comprensió dels textos.

Els poemes d’autors dels segles XIX i XX (així com la cançó popular *“A la vora de la mar”*) van acompanyats d’anotacions que aclareixen determinats aspectes lèxics dels poemes.

Cadascun dels poemes va precedit per un breu text explicatiu que ajuda a situar l’autor i la seva obra. Aquesta nota vol afavorir essencialment la lectura, sense pretendre la inserció de l’obra en un context històric.

En apèndix s’inclouen un seguit de poemes complementaris relacionats amb algunes de les activitats proposades per als poemes de l’antologia.
